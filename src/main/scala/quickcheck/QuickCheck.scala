package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  lazy val genHeap: Gen[H] = for {
    x <- arbitrary[Int]
    h <- frequency((1, const(empty)), (9, genHeap))
  } yield insert(x, h)

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)


  property("gen1") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h)) == m
  }


  property("is empty") = forAll { a: A =>
    !isEmpty(insert(a, empty))
  }

  property("is empty - for complex heaps") = forAll { a: A =>
    val first = a
    val second = first + 1
    val third = second + 1
    val heap = insert(third, insert(first, insert(second, empty)))
    !isEmpty(heap)
  }


  property("single insert with min") = forAll { a: A =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  property("min from two elements inserted in different order is correct") = forAll { (a: A, b: A) =>
    val lower = Math.min(a, b)
    val higher = Math.max(a, b)
    findMin(insert(lower, insert(higher, empty))) == lower && findMin(insert(higher, insert(lower, empty))) == lower
  }


  property("delete") = forAll { (a: A) =>
    isEmpty(deleteMin(insert(a, empty)))
  }

  property("delete from complex heap") = forAll { (a: A, b: A) =>
    !isEmpty(deleteMin(insert(a, insert(b, empty))))
  }

  property("is ordered recursive") = forAll { (h: H) =>
    def isSorted(heap: H): Boolean = {
      if (isEmpty(heap)) true
      else {
        val min = findMin(heap)
        val newHeap = deleteMin(heap)
        isEmpty(newHeap) || (min <= findMin(newHeap) && isSorted(newHeap))
      }
    }
    isSorted(h)
  }


  property("meld right empty") = forAll {(h1: H) =>
    meld( h1, empty ) == h1
  }

  property("meld left empty") = forAll {(h1: H) =>
    meld( empty , h1 ) == h1
  }

  property("meld should sort new heap") = forAll {(h1: H, h2:H) =>
    findMin( meld(  h1 , h2 ) ) == Math.min(findMin(h1),findMin(h2))
  }

  property("meld should contains all heaps elements") = forAll {(h1: H, h2:H) =>
    def checkHeaps(merged:H, left: H, right:H): Boolean ={
      if( isEmpty(merged) && isEmpty(left) && isEmpty(right)) true
      else if( !isEmpty(left)  && findMin(left)  == findMin(merged) ) checkHeaps(deleteMin(merged),deleteMin(left),right)
      else if( !isEmpty(right) && findMin(right) == findMin(merged) ) checkHeaps(deleteMin(merged),left,deleteMin(right))
      else false
    }
    checkHeaps(meld(  h1 , h2 ),h1,h2)
  }

  property("associative theorem meld")= forAll { (h1: H, h2: H, h3:H) =>
    findMin( meld( h1, meld(h2,h3)) ) == findMin(( meld(meld(h1,h2),h3)))

  }
}
